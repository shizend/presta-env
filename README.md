# Environment set up
```bash
# checkout env source
mkdir ~/presta & cd ~/presta
git clone {presta-env.git} env/

# checkout Presta source
git clone https://github.com/PrestaShop/PrestaShop.git www/

# creates virtual machine
cd env/
vagrant up

# ssh to machine
vagrant ssh

# build container
cd /vagrant/env/
docker-compose up -d
```

# Composer
```bash
# interact container
docker exec -it presta-www bash
cd /var/www/html
composer install
```

# Environment Info
- Machine IP: 10.51.0.8
- Apache: presta-www:80
- Mysql: presta-mysql:3360 root/admin