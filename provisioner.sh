#!/usr/bin/env bash

#enable ip4 forwarding
sudo sysctl -w net.ipv4.ip_forward=1

#Install docker
curl -fsSL get.docker.com | sh
sudo usermod -aG docker vagrant

#Install docker-compose
curl -L https://github.com/docker/compose/releases/download/1.17.1/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose